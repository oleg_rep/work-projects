function validate() {
    var debit = document.forms["form"]["dAcc"].value;
    var credit = document.forms["form"]["cAcc"].value;

    var dateFrom = document.forms["form"]["dateFrom"].value;
    var dateTo = document.forms["form"]["dateTo"].value;

    if (debit == null || debit == "") {
        document.getElementById("dt").innerHTML = " *счет дт обязательно к заполнению";
        return false;
    } else if (credit == null || credit == "") {
        document.getElementById("dt").innerHTML = "*счет кт обязательно к заполнению";
        return false;
    } else if (dateFrom == null || dateFrom == "" || dateTo == null || dateTo == "") {
        document.getElementById("dt").innerHTML = "*даты обязательно к заполнению";
        return false;
    } else if (credit.length < 14 && debit.length < 14) {
        document.getElementById("dt").innerHTML = "*Значение " + credit + " и " + debit + " не корректно, проверте на наличие лишних символов и количество знаков должно быть >= 14";
        return false;
    } else if (debit.length < 14) {
        document.getElementById("dt").innerHTML = "*Значение " + debit + " не корректно, проверте на наличие лишних символов и количество знаков должно быть >= 14";
        return false;
    } else if (credit.length < 14) {
        document.getElementById("dt").innerHTML = "*Значение " + credit + " не корректно, проверте на наличие лишних символов и количество знаков должно быть >= 14";
        return false;
    } else {
        return true;
    }
}