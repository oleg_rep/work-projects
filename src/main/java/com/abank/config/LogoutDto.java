package com.abank.config;

import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LogoutDto {
    private String error;
    private String errorDescription;
    private String redirectUri;
}
