package com.abank.config;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;

import javax.security.auth.Subject;
import java.util.Collection;
import java.util.List;

public class Oauth2AuthenticationAuthorityDecorator extends OAuth2Authentication {

    private List<GrantedAuthority> authorities;
    private OAuth2Authentication oAuth2Authentication;

    public Oauth2AuthenticationAuthorityDecorator(OAuth2Authentication auth2Authentication, List<GrantedAuthority> authorities) {
        super(auth2Authentication.getOAuth2Request(), auth2Authentication.getUserAuthentication());
        this.oAuth2Authentication = auth2Authentication;
        this.authorities = authorities;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    @Override
    public Object getCredentials() {
        return oAuth2Authentication.getCredentials();
    }

    @Override
    public Object getPrincipal() {
        return oAuth2Authentication.getPrincipal();
    }

    @Override
    public boolean isClientOnly() {
        return oAuth2Authentication.isClientOnly();
    }

    @Override
    public OAuth2Request getOAuth2Request() {
        return oAuth2Authentication.getOAuth2Request();
    }

    @Override
    public Authentication getUserAuthentication() {
        return oAuth2Authentication.getUserAuthentication();
    }

    @Override
    public boolean isAuthenticated() {
        return oAuth2Authentication.isAuthenticated();
    }

    @Override
    public void eraseCredentials() {
        oAuth2Authentication.eraseCredentials();
    }

    @Override
    public boolean equals(Object o) {
        return oAuth2Authentication.equals(o);
    }

    @Override
    public int hashCode() {
        return oAuth2Authentication.hashCode();
    }

    @Override
    public String getName() {
        return oAuth2Authentication.getName();
    }

    @Override
    public void setAuthenticated(boolean authenticated) {
        oAuth2Authentication.setAuthenticated(authenticated);
    }

    @Override
    public Object getDetails() {
        return oAuth2Authentication.getDetails();
    }

    @Override
    public void setDetails(Object details) {
        oAuth2Authentication.setDetails(details);
    }

    @Override
    public String toString() {
        return oAuth2Authentication.toString();
    }

    @Override
    public boolean implies(Subject subject) {
        return oAuth2Authentication.implies(subject);
    }
}
