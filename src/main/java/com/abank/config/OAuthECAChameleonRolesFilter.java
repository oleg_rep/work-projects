package com.abank.config;


import com.pb.chameleon.xml.subsystems.Subsystem;
import com.pb.chameleonserver.client.ChameleonServerClient;
import com.pb.chameleonserver.exception.ChameleonExecutionException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.filter.OncePerRequestFilter;
import ua.pb.security.chameleon.configuration.ChameleonProperties;
import ua.pb.security.chameleon.exceptions.AuthorizationException;
import ua.pb.security.chameleon.exceptions.IdentificationException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
class OAuthECAChameleonRolesFilter extends OncePerRequestFilter {

    private final ChameleonServerClient chameleonClient;
    private final ChameleonProperties chameleonProperties;
    private final AccessConfig accessConfig;
    private final boolean accessWithoutSid;

    private String getAuthToken() {
        return Optional.ofNullable(SecurityContextHolder.getContext())
                .map(SecurityContext::getAuthentication)
                .map(Authentication::getDetails)
                .filter(details -> details instanceof OAuth2AuthenticationDetails)
                .map(details -> (OAuth2AuthenticationDetails) details)
                .map(OAuth2AuthenticationDetails::getTokenValue)
                .orElse(null);
    }

    private List<Subsystem> getSubsystems() {
        return chameleonProperties.getSystems()
                .stream()
                .map(Subsystem::new)
                .collect(Collectors.toList());
    }

    private List<GrantedAuthority> getAuthorities(String sid) throws ChameleonExecutionException {
        List<String> roles = chameleonClient
                .getSessionActiveRoles(sid, getSubsystems())
                .stream()
                .map(r -> r.getSubsystemId() + ":" + r.getId())
                .collect(Collectors.toList());

        return roles
                .stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }

    private Set<String> mapRoleListToApplicationRoles(List<String> roles) {
        Set<String> appRoles = new HashSet<>();

        for (String role : roles) {
            if (accessConfig.getRead().contains(role)) appRoles.add("ACCESS_READ");
            if (accessConfig.getRepeat().contains(role)) appRoles.add("ACCESS_REPEAT");
            if (accessConfig.getControl().contains(role)) appRoles.add("ACCESS_CONTROL");
        }
        return appRoles;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {

        String sid = getAuthToken();

        if (sid == null) {
            if (accessWithoutSid) {
                filterChain.doFilter(httpServletRequest, httpServletResponse);
                return;
            } else {
                throw new IdentificationException("Не найдена сессия авторизации");
            }
        }

        Optional.ofNullable(SecurityContextHolder.getContext())
                .map(SecurityContext::getAuthentication)
                .filter(authentication -> authentication instanceof OAuth2Authentication)
                .map(authentication -> (OAuth2Authentication) authentication)
                .ifPresent(authentication -> {
                    try {

                        if (authentication instanceof Oauth2AuthenticationAuthorityDecorator) return;

                        List<GrantedAuthority> authorities = getAuthorities(sid);
                        authentication = new Oauth2AuthenticationAuthorityDecorator(authentication, authorities);
                        SecurityContextHolder.getContext().setAuthentication(authentication);
                        System.out.println(authentication.getName());
                    } catch (ChameleonExecutionException e) {
                        throw new AuthorizationException("Ошибка получения ролей пользователя", e);
                    }
                });
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}

