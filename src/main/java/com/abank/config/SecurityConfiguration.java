package com.abank.config;


import com.pb.chameleonserver.client.ChameleonServerClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;
import ua.pb.security.chameleon.configuration.ChameleonProperties;

import java.util.Collections;

@Configuration
@EnableWebSecurity
@Slf4j
@ConditionalOnExpression("${app-config.ui-enabled:true}")
@EnableConfigurationProperties({AccessConfig.class})
public class SecurityConfiguration {

    @Configuration
    @Order(3)
    @RequiredArgsConstructor
    public static class ApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {
        private final ChameleonProperties cham;
        private final AccessConfig accessConfig;

        private final HandlerExceptionResolver handlerExceptionResolver;
        private final ChameleonServerClient chameleonServerClient;

        protected void configure(HttpSecurity http) throws Exception {
            log.info("Configure http1");

            OAuthECAChameleonRolesFilter oAuthECAChameleonRolesFilter
                    = new OAuthECAChameleonRolesFilter(chameleonServerClient, cham, accessConfig, false);

            http.antMatcher("/api/**")
                    .authorizeRequests()
                    .anyRequest().authenticated()
                    .and()
                    .addFilterBefore(oAuthECAChameleonRolesFilter, UsernamePasswordAuthenticationFilter.class)
                    .csrf().disable();
        }
    }

    @Configuration
    @EnableConfigurationProperties(ECAOauthProperties.class)
    @RequiredArgsConstructor
    @EnableOAuth2Sso
    public static class MainWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

        private final ChameleonProperties cham;
        private final ChameleonServerClient chameleonServerClient;
        private final AccessConfig accessConfig;
        private final ECAOauthProperties clientProperties;

        @Override
        protected void configure(HttpSecurity http) throws Exception {

            log.info("props,{}", cham);
            OAuthECAChameleonRolesFilter oAuthECAChameleonRolesFilter
                    = new OAuthECAChameleonRolesFilter(chameleonServerClient, cham, accessConfig, true);

            log.info("Configure http2");
            http.anonymous().disable();
            http.requestMatcher(r -> !r.getRequestURI().startsWith(r.getContextPath() + "/api"))
                    .authorizeRequests()
                    .antMatchers("/unauthenticated", "/login", "/login**", "/error**").permitAll()
                    .anyRequest().authenticated()
                    .and().logout()
                    .logoutSuccessUrl("/")
                    .addLogoutHandler(new ECALogoutHandler(clientProperties))
                    .permitAll()
                    .and()
                    .addFilterBefore(oAuthECAChameleonRolesFilter, UsernamePasswordAuthenticationFilter.class)
                    .csrf().disable();
        }
    }
    @Bean
    PrincipalExtractor principalExtractor() {
        return map -> new User(map.get("username").toString(), "", Collections.emptySet());
    }
}
