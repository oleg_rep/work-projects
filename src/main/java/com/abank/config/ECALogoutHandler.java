package com.abank.config;

import com.pb.chameleon.xml.subsystems.Subsystem;
import com.pb.chameleonserver.client.ChameleonServerClient;
import com.pb.chameleonserver.exception.ChameleonExecutionException;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.OncePerRequestFilter;
import ua.pb.security.chameleon.configuration.ChameleonProperties;
import ua.pb.security.chameleon.exceptions.AuthorizationException;
import ua.pb.security.chameleon.exceptions.IdentificationException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;



@RequiredArgsConstructor
@Slf4j
public class ECALogoutHandler implements LogoutHandler {

    private final ECAOauthProperties ecaOauthProperties;

    private void doLogout(OAuth2AuthenticationDetails details, String redirectPath){
        RestTemplate restTemplate = new RestTemplate();

        Map<String, String> params = new HashMap<>();
        params.put("client_id", ecaOauthProperties.getClientId());
        params.put("scope", "read");
        params.put("start_uri", redirectPath);

        HttpHeaders headers = new HttpHeaders();
        headers.put("Authorization", Collections.singletonList("Bearer "+details.getTokenValue()));


        LogoutDto logoutDto = restTemplate.exchange(
                ecaOauthProperties.getEcaLogoutUrl(),
                HttpMethod.POST,
                new HttpEntity<>(headers),
                LogoutDto.class,
                params
        ).getBody();
    }

    @Override
    public void logout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) {
        Optional.ofNullable(SecurityContextHolder.getContext())
                .map(SecurityContext::getAuthentication)
                .map(Authentication::getDetails)
                .filter(details -> details instanceof OAuth2AuthenticationDetails)
                .map(details -> (OAuth2AuthenticationDetails) details)
                .ifPresent(d->doLogout(d,httpServletRequest.getContextPath()));
    }
}









