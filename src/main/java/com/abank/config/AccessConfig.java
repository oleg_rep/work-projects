package com.abank.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.LinkedList;
import java.util.List;

@ConfigurationProperties("app-config.access")
@Data
public class AccessConfig {
    List<String> read = new LinkedList<>();
    List<String> repeat = new LinkedList<>();
    List<String> control = new LinkedList<>();
}
