package com.abank.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "security.oauth2.client")
public
class ECAOauthProperties {
    private String clientId;
    private String ecaLogoutUrl;

}
