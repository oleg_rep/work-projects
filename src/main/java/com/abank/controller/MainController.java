package com.abank.controller;

import com.abank.model.Deal;
import com.abank.model.Ordering;
import com.abank.service.*;
import com.abank.view.ExcelOrderingListReportView;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Controller
public class MainController {
    private final OrderingDaoService orderingService;

    private List<Ordering> orderingList;
    private List<Deal> dealList;

    private static NumberFormat nFormat = DecimalFormat.getNumberInstance();

    @GetMapping(value = "/index")
    @PreAuthorize("permitAll()")
    public String index() {
        System.out.println(orderingService.getDealInfo(10836713).getDeals());
        return "index";
    }

    @GetMapping(value = "/account_info/{account}")
    public String getAccountInfo(@PathVariable("account") String account, Model model) {
        model.addAttribute("msg", account);
        return "account_info";
    }

    @GetMapping(value = "find_by_acc_debet_credet_get")
    public String findByAccDebetCredetGet(Model model) {
        model.addAttribute("ordering", new Ordering());
        return "find_by_acc_debet_credet_get";
    }

    @PostMapping(value = "/find_by_acc_debet_credet_post")
    public String findByAccDebetCredetPost(@ModelAttribute Ordering ordering, ModelMap model) {
        OrderingResponse orderingResponse = orderingService.getByDtKtService(ordering.getDAcc(), ordering.getCAcc(), ordering.getDateFrom(), ordering.getDateTo());
        orderingList = orderingResponse.getOrderings();
        model.addAttribute("sum", nFormat.format(orderingResponse.getOrderingSum()));
        model.addAttribute("count", orderingResponse.getOrderingCount());
        model.addAttribute("findByAccView", orderingList);
        model.addAttribute("beginOnv", nFormat.format(orderingResponse.getBalanceBeginSum()));
        model.addAttribute("endOnv", nFormat.format(orderingResponse.getBalanceEndSum()));
        model.addAttribute("obd", nFormat.format(orderingResponse.getBalanceObd()));
        model.addAttribute("obk", nFormat.format(orderingResponse.getBalanceObk()));

        if (!orderingList.isEmpty()) {
            log.info("Search by {} -> {}  dateFrom: {} dateTo: {} successful sum of ordering - {} count - {}",
                    ordering.getDAcc(),
                    ordering.getCAcc(),
                    ordering.getDateFrom(),
                    ordering.getDateTo(),
                    orderingResponse.getOrderingSum(),
                    orderingResponse.getOrderingCount()
            );
        } else {
            log.info("Accounts {} -> {} in period {} - {} not found",
                    ordering.getDAcc(),
                    ordering.getCAcc(),
                    ordering.getDateFrom(),
                    ordering.getDateTo()
            );
        }
        return "find_by_acc_debet_credet_post";
    }

    @GetMapping(value = "/deal_report_get")
    public String dealReportGet(Model model) {
            model.addAttribute("dealsView", new Deal());
            return "deal_report_get";
    }

    @PostMapping(value = "/deal_report_post")
    public String dealReportPost(@ModelAttribute Deal deal, ModelMap model) {
        DealResponse dealResponse = orderingService.getDealInfo(deal.getTreatyId());
        dealList = dealResponse.getDeals();

        model.addAttribute("eosDlr", dealResponse.getDeals().get(0).getEosDlr());
        model.addAttribute("treatyId", deal.getTreatyId());
        model.addAttribute("dealsPost", dealList);
        return "deal_report_post";
    }

    @RequestMapping(value = "/export_to_xls_dt", method = RequestMethod.GET)
    public ModelAndView exportToExcelDebit(HttpServletRequest req) {
        String typeReport = req.getParameter("type");

        if (typeReport != null && typeReport.equals("xls")) {
            return new ModelAndView(new ExcelOrderingListReportView(), "findByDView", orderingList);
        }
        return new ModelAndView("export_to_xls_dt", "findByDtView", orderingList);
    }
}