package com.abank.controller;


import com.abank.model.Report01xSum;
import com.abank.service.ReportDaoService;
import com.abank.service.ReportResponse;
import com.abank.view.ReportXls;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Slf4j
@Controller
@RequiredArgsConstructor
public class ReportController {

    private final ReportDaoService reportService;
    private List<Report01xSum> reportList;


    @RequestMapping(value = "/report_01x_sum_get", method = RequestMethod.GET)
    public String report(Model model,@ModelAttribute Report01xSum report01xSum) {
        model.addAttribute("report_01_sum_view", new Report01xSum());
        return "report_01x_sum_get";
    }

    @RequestMapping(value = "/report_01x_sum_post", method = RequestMethod.POST)
    public String report(@ModelAttribute Report01xSum report01xSum, ModelMap model) {
        ReportResponse reportResponse = reportService.getReport01xSum(report01xSum.getDatePrev(), report01xSum.getDatePrevCorrFrom(),
                report01xSum.getDatePrevCorrTo(), report01xSum.getDateCurr(),
                report01xSum.getDateCurrCorrFrom(), report01xSum.getDatePrevCorrTo());

        reportList = reportResponse.getReport01xSums();
        model.addAttribute("report_01_sum_view", reportList);
        log.info("Search by ", reportResponse.getReport01xSums());
        return "report_01x_sum_post";
    }

    @RequestMapping(value = "/report_to_excel", method = RequestMethod.GET)
    public ModelAndView exportToExcelReport(HttpServletRequest req) {
        String typeReport = req.getParameter("type_rep");
        if (typeReport != null && typeReport.equals("xls")) {
            return new ModelAndView(new ReportXls(), "repToXls", reportList);
        }
        return new ModelAndView("report_to_excel", "repToXls", reportList);
    }
}
