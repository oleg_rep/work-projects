package com.abank.service;

import com.abank.model.Report01xSum;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class ReportResponse {
    private List<Report01xSum> report01xSums;
}
