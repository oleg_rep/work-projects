package com.abank.service;

import com.abank.dao.OrderingDao;
import com.abank.model.Report01xSum;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ReportDaoServiceImpl implements ReportDaoService {

    private final OrderingDao dao;

    @Override
    public ReportResponse getReport01xSum(String datePrev, String datePrevCorrFrom, String datePrevCorrTo, String dateCurr,
                                          String dateCurrCorrFrom, String dateCurrCorrTo) {
        List<Report01xSum> report01xSums = dao.getReport01xSum(
                datePrevCorrFrom, datePrevCorrTo, datePrev,
                dateCurrCorrFrom, dateCurrCorrTo, dateCurr);
        return ReportResponse.builder()
                .report01xSums(report01xSums)
                .build();
    }
}
