package com.abank.service;

import com.abank.model.Balance;
import com.abank.model.Ordering;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class OrderingResponse {
    private List<Ordering> orderings;
    private List<Balance> balances;
    private Double orderingSum;
    private Double balanceBeginSum;
    private Double balanceEndSum;
    private Double balanceObd;
    private Double balanceObk;
    private Integer orderingCount;
}
