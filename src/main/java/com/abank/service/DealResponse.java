package com.abank.service;


import com.abank.model.Deal;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Builder
public class DealResponse {
    private List<Deal> deals;
}
