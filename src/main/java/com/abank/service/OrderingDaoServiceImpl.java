package com.abank.service;

import com.abank.dao.OrderingDao;
import com.abank.model.Balance;
import com.abank.model.Deal;
import com.abank.model.Ordering;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderingDaoServiceImpl implements OrderingDaoService {

    private final OrderingDao dao;

    @Override
    public OrderingResponse getByDtKtService(String dAcc, String cAcc, String dateFrom, String dateTo) {
        List<Ordering> orderings = dao.getByDtKt(dAcc, cAcc, dateFrom, dateTo);
        List<Balance> balance = dao.getBalance(dAcc, dateFrom, dateTo);

//        System.out.println(dao.getAccount());

        double sum = orderings.stream()
                .mapToDouble(Ordering::getBpl_sum)
                .sum();

        double sumBalanceBegin = 0;
        double sumBalanceEnd = 0;
        double obd = 0;
        double obk = 0;

        try {
            sumBalanceBegin = balance.get(0).getOnv();
            sumBalanceEnd = balance.get(balance.size() - 1).getOnv();
            obd = balance.stream()
                    .mapToDouble(Balance::getObd)
                    .sum();

            obk = balance.stream()
                    .mapToDouble(Balance::getObk)
                    .sum();
        } catch (IndexOutOfBoundsException i) {
            System.out.println("");
        }

        return OrderingResponse.builder()
                .orderings(orderings)
                .orderingCount(orderings.size())
                .orderingSum(sum)
                .balanceBeginSum(sumBalanceBegin)
                .balanceEndSum(sumBalanceEnd)
                .balanceObd(obd)
                .balanceObk(obk)
                .build();
    }

    @Override
    public DealResponse getDealInfo(Integer treatyId) {
        List<Deal> dealList = dao.getDeals(treatyId);
        return DealResponse.builder().deals(dealList).build();
    }
}
