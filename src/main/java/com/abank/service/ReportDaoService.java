package com.abank.service;

public interface ReportDaoService {
    ReportResponse getReport01xSum(String datePrev, String datePrevCorrFrom, String datePrevCorrTo, String dateCurr,
                                   String dateCurrCorrFrom, String dateCurrCorrTo);
}
