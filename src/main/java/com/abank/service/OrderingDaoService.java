package com.abank.service;

public interface OrderingDaoService {
    OrderingResponse getByDtKtService(String dAcc, String cAcc, String dateFrom, String dateTo);

    DealResponse getDealInfo(Integer treatyId);


//    int countDtKt(String dAcc, String cAcc, String dateFrom, String dateTo);
//
//    float sumDt(String dAcc, String cAcc, String dateFrom, String dateTo);
//
//    float getBalanceByAccService();
//
//    float getDebetTurnoverService();
//
//    float getCreditTurnoverService();

}
