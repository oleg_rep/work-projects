package com.abank.dao_mapper;

import com.abank.model.Report01xSum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class AccountRowMapper implements RowMapper<Report01xSum> {




    @Override
    public Report01xSum mapRow(ResultSet row, int rowNum) throws SQLException {
        Report01xSum account = new Report01xSum();
        account.setId(row.getInt("id"));
        account.setNameReport(row.getString("name_report"));
        account.setSumPrev(row.getString("sum_prev"));
        account.setCountPrev(row.getString("count_prev"));
        account.setProsSumPrev(row.getString("pros_sum_prev"));
        account.setProsCountPrev(row.getString("pros_count_prev"));
        account.setSumCurent(row.getString("sum_curent"));
        account.setCountCurent(row.getString("count_curent"));
        account.setProsSumCurent(row.getString("pros_sum_curent"));
        account.setProsCountCurent(row.getString("pros_count_curent"));
        account.setDiffSumAcc(row.getString("diff_sum_acc"));
        account.setDiffCountAcc(row.getString("diff_count_acc"));
        account.setDiffSumPros(row.getString("diff_sum_pros"));
        account.setDiffCountPros(row.getString("diff_count_pros"));
        return account;
    }
}
