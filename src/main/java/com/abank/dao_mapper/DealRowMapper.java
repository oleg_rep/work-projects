package com.abank.dao_mapper;

import com.abank.model.Balance;
import com.abank.model.Deal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class DealRowMapper implements RowMapper<Deal> {


    @Override
    public Deal mapRow(ResultSet row, int rowNum) throws SQLException {
        Deal deal = new Deal();

        deal.setEosDlr(row.getString("eos_dlr"));
        deal.setOpenAcc(row.getString("open_acc"));
        deal.setKind(row.getString("Kind"));
        deal.setTreatyId(row.getInt("TreatyID"));
        deal.setMoniker(row.getString("Moniker"));
        deal.setIban(row.getString("iban"));
        deal.setName(row.getString("name"));
        return deal;
    }
}
