package com.abank.dao_mapper;

import com.abank.model.Balance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class BalanceRowMapper implements RowMapper<Balance> {



    @Override
    public Balance mapRow(ResultSet row, int rowNum) throws SQLException {
        Balance balance = new Balance();

        balance.setBmoAcc(row.getString("bmo_acc"));
        balance.setBmoDatOd(row.getString("bmo_dat_od"));
        balance.setBmoCcy(row.getString("bmo_ccy"));
        balance.setOnv(row.getDouble("onv"));
        balance.setObdv(row.getDouble("obdv"));
        balance.setObkv(row.getDouble("obkv"));
        balance.setOn(row.getDouble("on"));
        balance.setObd(row.getDouble("obd"));
        balance.setObk(row.getDouble("obk"));
        balance.setDpd(row.getString("dpd"));

        return balance;
    }
}
