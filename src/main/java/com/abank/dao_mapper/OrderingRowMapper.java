package com.abank.dao_mapper;

import com.abank.model.Ordering;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class OrderingRowMapper implements RowMapper<Ordering> {



    @Override
    public Ordering mapRow(ResultSet row, int rowNum) throws SQLException {
        Ordering ordering = new Ordering();

        ordering.setBplRef(row.getString("bpl_ref"));
        ordering.setBplRefn(row.getString("bpl_refn"));
        ordering.setBplDocTyp(row.getString("bpl_doc_typ"));
        ordering.setBplNumDoc(row.getString("bpl_num_doc"));
        ordering.setBplPrOut(row.getString("bpl_pr_out"));
        ordering.setDateOdb(row.getString("bpl_dat_od"));
        ordering.setCcy(row.getString("bpl_ccy"));
        ordering.setDAcc(row.getString("bpl_a_acc"));
        ordering.setDName(row.getString("bpl_a_nam"));
        ordering.setDOkpo(row.getString("bpl_a_crf"));
        ordering.setDMfo(row.getString("bpl_a_mfo"));
        ordering.setCAcc(row.getString("bpl_b_acc"));
        ordering.setCName(row.getString("bpl_b_nam"));
        ordering.setCOkpo(row.getString("bpl_b_crf"));
        ordering.setCMfo(row.getString("bpl_b_mfo"));
        ordering.setAAcc(row.getString("bpl_a_acc"));
        ordering.setBAcc(row.getString("bpl_b_acc"));
        ordering.setOsnd(row.getString("bpl_osnd"));
        ordering.setBpl_sum(row.getDouble("bpl_sum"));
        ordering.setBplSumE(row.getString("bpl_sum_e"));
        ordering.setBplBrnm(row.getString("bpl_brnm"));
        return ordering;
    }
}
