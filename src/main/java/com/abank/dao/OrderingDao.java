package com.abank.dao;

import com.abank.model.Deal;
import com.abank.model.Report01xSum;
import com.abank.model.Balance;
import com.abank.model.Ordering;

import java.util.List;

public interface OrderingDao {

    List<Ordering> getByDtKt(String dAcc, String cAcc, String dateFrom, String dateTo);

    List<Balance> getBalance(String acc, String dateOdbBegin, String dateOdbEnd);

    List<Report01xSum> getReport01xSum(String datePrev, String datePrevCorrFrom, String datePrevCorrTo, String dateCurr,
                                       String dateCurrCorrFrom, String dateCurrCorrTo);

    List<Deal> getDeals(Integer treatyID);

}
