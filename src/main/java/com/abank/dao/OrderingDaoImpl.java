package com.abank.dao;

import com.abank.dao_mapper.AccountRowMapper;
import com.abank.dao_mapper.BalanceRowMapper;
import com.abank.dao_mapper.DealRowMapper;
import com.abank.dao_mapper.OrderingRowMapper;
import com.abank.model.Deal;
import com.abank.model.Report01xSum;
import com.abank.model.Balance;
import com.abank.model.Ordering;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@RequiredArgsConstructor
@Repository
public class OrderingDaoImpl implements OrderingDao {

    //    IQ
    @Resource(name = "primaryDSJdbcTemplate")
    private final JdbcTemplate jdbcTemplate;
    //  MSSQL
    @Resource(name = "secondaryDsJdbcTemplate")
    private final JdbcTemplate jdbcTemplate2;

    @Autowired
    private RowMapper<Ordering> orderingRowMapper;

    @Autowired
    private RowMapper<Balance> balanceRowMapper;

    @Autowired
    private RowMapper<Report01xSum> accountRowMapper;

    @Autowired
    private RowMapper<Deal> dealRowMapper;

    @Override
    public List<Ordering> getByDtKt(String dAcc, String cAcc, String dateFrom, String dateTo) {
        String year = dateFrom.substring(0, 4);

        String sql = "select top 10000 " +
                "bpl_ref,bpl_refn,bpl_doc_typ,bpl_num_doc," +
                "bpl_pr_out,bpl_dat_od,bpl_ccy,bpl_a_acc," +
                "bpl_a_nam,bpl_a_crf,bpl_a_mfo,bpl_b_acc," +
                "bpl_b_nam,bpl_b_crf,bpl_b_mfo,bpl_a_acc,bpl_b_acc," +
                "bpl_osnd,bpl_sum,bpl_sum_e,bpl_brnm " +
                "from oper.plpb_" + year + " where (bpl_a_acc = ?1 or bpl_b_acc = ?2) and BPL_DAT_OD >= ?3 and BPL_DAT_OD <= ?4 and bpl_pr_pr = 'r' and bpl_fl_real = 'r'" +
                " order by BPL_DAT_OD";

        return jdbcTemplate.query(sql, new Object[]{dAcc.trim(), cAcc.trim(), dateFrom, dateTo}, orderingRowMapper);
    }

    @Override
    public List<Balance> getBalance(String acc, String dateOdbBegin, String dateOdbEnd) {
        String sql = "select bmo_acc,bmo_dat_od,bmo_ccy," +
                "onv,obdv,obkv,[on],obd," +
                "obk,dpd " +
                "from oper.morp " +
                "where bmo_acc = ?1 and bmo_dat_od >= ?2 and bmo_dat_od <= ?3" +
                " order by bmo_dat_od";
        return jdbcTemplate.query(sql, new Object[]{acc.trim(), dateOdbBegin, dateOdbEnd}, balanceRowMapper);
    }

    @Override
    public List<Report01xSum> getReport01xSum(String datePrev, String datePrevCorrFrom, String datePrevCorrTo, String dateCurr,
                                              String dateCurrCorrFrom, String dateCurrCorrTo) {
        try (Connection conn = jdbcTemplate2.getDataSource().getConnection()) {
            CallableStatement exec = conn.prepareCall("{call odb.dbo.report02x (?,?,?,?,?,?)}");
            exec.setString(1, datePrev);
            exec.setString(2, datePrevCorrFrom);
            exec.setString(3, datePrevCorrTo);
            exec.setString(4, dateCurr);
            exec.setString(5, dateCurrCorrFrom);
            exec.setString(6, dateCurrCorrTo);
            exec.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        String sql = "select id,name_report,sum_prev,count_prev,pros_sum_prev,\n" +
                " pros_count_prev,sum_curent,count_curent,pros_sum_curent,\n" +
                " pros_count_curent,diff_sum_acc,diff_count_acc,\n" +
                " diff_sum_pros,diff_count_pros from odb..report";

        return jdbcTemplate2.query(sql, accountRowMapper);
    }

    @Override
    public List<Deal> getDeals(Integer treatyID) {
        try (Connection conn = jdbcTemplate.getDataSource().getConnection()) {
            CallableStatement exec = conn.prepareCall("{call ab050984kos.odb_ordering_deals(?)}");
            exec.setInt(1, treatyID);
            exec.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String sql = "select eos_dlr,open_acc,kind,TreatyID,MONIKER,IBAN, Name from odb_ordering_deals";

        return jdbcTemplate.query(sql, dealRowMapper);
    }
}