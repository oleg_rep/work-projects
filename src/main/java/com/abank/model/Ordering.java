package com.abank.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
public class Ordering {
    private String bplRef;
    private String bplRefn;
    private String bplDocTyp;
    private String bplPrOut;
    private String bplNumDoc;
    private String bplBrnm;
    private String bplSumE;
    private String dateOdb;
    private String dateFrom;
    private String dateTo;
    private String aAcc;
    private String dAcc;
    private String dName;
    private String dOkpo;
    private String dMfo;
    private String bAcc;
    private String cAcc;
    private String cName;
    private String cOkpo;
    private String cMfo;
    private String ccy;
    private String osnd;
    private Double bpl_sum;
}
