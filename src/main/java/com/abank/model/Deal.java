package com.abank.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class Deal {
    private String eosDlr;
    private String openAcc;
    private String kind;
    private Integer treatyId;
    private String moniker;
    private String iban;
    private String name;
}
