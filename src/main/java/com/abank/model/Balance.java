package com.abank.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class Balance {
    private String bmoAcc;
    private String bmoDatOd;
    private String bmoCcy;
    private Double onv;
    private Double obdv;
    private Double obkv;
    private Double on;
    private Double obd;
    private Double obk;
    private String dpd;
}
