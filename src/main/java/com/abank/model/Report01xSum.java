package com.abank.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class Report01xSum {
    private Integer id;
    private String nameReport;
    private String sumPrev;
    private String countPrev;
    private String prosSumPrev;
    private String prosCountPrev;
    private String sumCurent;
    private String countCurent;
    private String prosSumCurent;
    private String prosCountCurent;
    private String diffSumAcc;
    private String diffCountAcc;
    private String diffSumPros;
    private String diffCountPros;
    //    ----------------------------
    private String datePrev;
    private String datePrevCorrFrom;
    private String datePrevCorrTo;

    private String dateCurr;
    private String dateCurrCorrFrom;
    private String dateCurrCorrTo;
}
