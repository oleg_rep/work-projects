package com.abank.view;

import com.abank.model.Ordering;
import com.abank.model.Report01xSum;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Color;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public class ReportXls extends AbstractXlsView {
    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setHeader("Content-disposition", "attachment; filename=\"ordering.xls\"");
        List<Report01xSum> list = (List<Report01xSum>) model.get("repToXls");


        Sheet sheet = workbook.createSheet("Report");
        Row header = sheet.createRow(0);
//        header.setRowStyle(header.getRowStyle());


        header.createCell(0).setCellValue("Наименование отчета");
        header.createCell(1).setCellValue("Сумма пред");
        header.createCell(2).setCellValue("Кол-во пред");
        header.createCell(3).setCellValue("Просрочка пред");
        header.createCell(4).setCellValue("кол-во прос пред");
        header.createCell(5).setCellValue("сумма тек");
        header.createCell(6).setCellValue("кол-во тек");
        header.createCell(7).setCellValue("просрочка тек");
        header.createCell(8).setCellValue("кол-во просрочек тек");
        header.createCell(9).setCellValue("Разница по суммам");
        header.createCell(10).setCellValue("Разница по количеству");
        header.createCell(11).setCellValue("Разница по сумме просрочки");
        header.createCell(12).setCellValue("Разница по кол-ву просрочек");



        int rowNum = 1;

        for (Report01xSum rep : list) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(rep.getNameReport());
            row.createCell(1).setCellValue(rep.getSumPrev());
            row.createCell(2).setCellValue(rep.getCountPrev());
            row.createCell(3).setCellValue(rep.getProsSumPrev());
            row.createCell(4).setCellValue(rep.getProsCountPrev());
            row.createCell(5).setCellValue(rep.getSumCurent());
            row.createCell(6).setCellValue(rep.getCountCurent());
            row.createCell(7).setCellValue(rep.getProsSumCurent());
            row.createCell(8).setCellValue(rep.getProsCountCurent());
            row.createCell(9).setCellValue(rep.getDiffSumAcc());
            row.createCell(10).setCellValue(rep.getDiffCountAcc());
            row.createCell(11).setCellValue(rep.getDiffSumPros());
            row.createCell(12).setCellValue(rep.getDiffCountPros());
        }
    }
}
