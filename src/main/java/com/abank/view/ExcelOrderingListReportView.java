package com.abank.view;

import com.abank.model.Ordering;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public class ExcelOrderingListReportView extends AbstractXlsView {
    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setHeader("Content-disposition", "attachment; filename=\"ordering.xls\"");

        List<Ordering> list = (List<Ordering>) model.get("findByDView");

        Sheet sheet = workbook.createSheet("Ordering List");

        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Референс");
        header.createCell(1).setCellValue("Дата");
        header.createCell(2).setCellValue("Дебет");
        header.createCell(3).setCellValue("Наименование");

        header.createCell(4).setCellValue("ОКПО");
        header.createCell(5).setCellValue("МФО");
        header.createCell(6).setCellValue("Кредит");
        header.createCell(7).setCellValue("Наименование");

        header.createCell(8).setCellValue("ОКПО");
        header.createCell(9).setCellValue("МФО");
        header.createCell(10).setCellValue("Валюта");
        header.createCell(11).setCellValue("Описание");
        header.createCell(12).setCellValue("Сумма");

        int rowNum = 1;

        for (Ordering ordering : list) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(ordering.getBplRef());
            row.createCell(1).setCellValue(ordering.getDateOdb());
            row.createCell(2).setCellValue(ordering.getDAcc());
            row.createCell(3).setCellValue(ordering.getDName());

            row.createCell(4).setCellValue(ordering.getDOkpo());
            row.createCell(5).setCellValue(ordering.getDMfo());
            row.createCell(6).setCellValue(ordering.getCAcc());
            row.createCell(7).setCellValue(ordering.getCName());

            row.createCell(8).setCellValue(ordering.getCOkpo());
            row.createCell(9).setCellValue(ordering.getCMfo());
            row.createCell(10).setCellValue(ordering.getCcy());
            row.createCell(11).setCellValue(ordering.getOsnd());
            row.createCell(12).setCellValue(ordering.getBpl_sum());
        }
    }
}